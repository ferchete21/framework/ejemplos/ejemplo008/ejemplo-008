<?php
use yii\widgets\ListView;
?>


<?= ListView::widget([
        'dataProvider' => $dataProvider,
    
        'columns' => [
            'colums' => $dataProvider,

            'titulo',
            'contenido',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>