<?php

namespace app\models;

use Yii;
//
//Model es cuando queremos hacer un formulario
//\Codeception\Lib\Interfaces\ActiveRecord:: es cuando queremos hacer una consulta
class Notas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'], 'string', 'max' => 100],
            [['contenido'], 'string', 'max' => 800],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'contenido' => 'Contenido',
        ];
    }
}
